package ntnu.adriawh;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import ntnu.adriawh.model.PatientRegister;

import java.io.IOException;
import java.util.Optional;

/**
 * JavaFX App
 */
public  class App extends Application {

    private static  PatientRegister patientRegister = new PatientRegister();

    public static PatientRegister getPatientRegister() {
        return patientRegister;
    }

    public static void setPatientRegister(PatientRegister patientRegister) {
        App.patientRegister = patientRegister;
    }

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("main" + ".fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setScene(scene);
        stage.setOnCloseRequest(event ->{
            exit();
            event.consume();
        });
        stage.show();
    }

    /**
     * Method called when the system is to close. User is met with confirmation dialog to
     * confirm if this is intended.
     */
    public static void exit(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Exit confirmation");
        alert.setHeaderText("Are you sure you want to exit the application");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            System.exit(1);
        }
    }
}