package ntnu.adriawh.factory;

public class GuiFactory {
    public static GuiObject getGuiObject(String objectType){
        switch (objectType){
            case("MENU_BAR"):
                return new MenuBarObject();
            case("TOOL_BAR"):
                return new ToolBarObject();
            case("TABLE_VIEW"):
                return new TableViewObject();
            default:
                return null;
        }
    }
}
