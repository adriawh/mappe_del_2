package ntnu.adriawh.factory;

import javafx.scene.Node;
import javafx.scene.control.ToolBar;

public class ToolBarObject extends GuiObject{

    @Override
    public Node createObject() {
        return new ToolBar();
    }
}
