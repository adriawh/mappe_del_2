package ntnu.adriawh.factory;

import javafx.scene.Node;
import javafx.scene.control.TableView;

public class TableViewObject extends GuiObject{

    @Override
    public Node createObject() {
        return new TableView<>();
    }
}
