package ntnu.adriawh.factory;

import javafx.scene.Node;
import javafx.scene.control.MenuBar;

public class MenuBarObject extends GuiObject {

    @Override
    public Node createObject() {
        return new MenuBar();
    }
}
