package ntnu.adriawh.factory;

import javafx.scene.Node;

public abstract class GuiObject {
    public abstract Node createObject();
}
