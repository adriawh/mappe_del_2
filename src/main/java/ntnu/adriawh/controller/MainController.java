package ntnu.adriawh.controller;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ntnu.adriawh.App;
import ntnu.adriawh.exceptions.AddException;
import ntnu.adriawh.exceptions.FileTypeException;
import ntnu.adriawh.exceptions.RemoveException;
import ntnu.adriawh.model.Patient;
import ntnu.adriawh.model.PatientRegister;
import ntnu.adriawh.persistance.CSVReader;
import ntnu.adriawh.persistance.CSVWriter;
import ntnu.adriawh.view.NoPatientSelectedAlert;
import ntnu.adriawh.view.PatientDialog;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class MainController {

    @FXML
    private TableView<Patient> tableView;

    @FXML
    private TableColumn<Patient, String> firstNameColumn;

    @FXML
    private TableColumn<Patient, String> lastNameColumn;

    @FXML
    private TableColumn<Patient, String> socialSecurityNumberColumn;

    @FXML
    private TableColumn<Patient, String> diagnosisColumn;

    @FXML
    private TableColumn<Patient, String> generalPractitionerColumn;

    @FXML
    private Text status;


    public void initialize(){
        //Double-click on a patient will make an information dialog appear
        tableView.setOnMousePressed(mouseEvent -> {
            if (mouseEvent.isPrimaryButtonDown() && (mouseEvent.getClickCount() == 2)) {
                PatientDialog patientDialog = new PatientDialog(tableView.getSelectionModel().getSelectedItem(), false);
                patientDialog.showAndWait();
            }
        });
        columnFactory();
        updateTableView();
    }


    /**
     * Method that shows a dialog about the application to the user
     */
    @FXML
    private void aboutDialog(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About:");
        alert.setHeaderText("Patient register \n V0.1-SNAPSHOT" );
        alert.setContentText("This application was made by: \nAdrian Wist Hakvåg \n\n" + "Assignment in IDATT2001");
        alert.showAndWait();
    }

    /**
     * Shows a dialog to add a patient.
     * If the patient could not be added the user is notified with why.
     */
    @FXML
    private void addPatient(){
        PatientDialog patientDialog = new PatientDialog();
        Optional<Patient> result = patientDialog.showAndWait();
        if(result.isPresent()){
            Patient patient = result.get();
            PatientRegister updated = App.getPatientRegister();
            try{
                updated.addPatient(patient);
                App.setPatientRegister(updated);
                updateTableView();
                status.setText("Patient added");
            }catch (AddException | IllegalArgumentException e ){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Could not add");
                alert.setHeaderText("Could not add the said Patient" );
                alert.setContentText(e.getMessage());
                alert.showAndWait();
            }
        }
    }

    /**
     * Shows an edit dialog for the selected patient in the tableView.
     * If no patient is selected, the user is made aware of this with an alert.
     */
    @FXML
    private void editPatient(){
        Patient selectedPatient = tableView.getSelectionModel().getSelectedItem();
        if( selectedPatient == null){
            new NoPatientSelectedAlert().showAndWait();
        }else{
            PatientDialog patientDialog = new PatientDialog(selectedPatient, true);
            Optional<Patient> result = patientDialog.showAndWait();
            if(result.isPresent()){
                Patient updatedPatient = result.get();
                PatientRegister updated = App.getPatientRegister();
                try{
                    updated.updatePatient(selectedPatient, updatedPatient);
                    App.setPatientRegister(updated);
                    updateTableView();
                    status.setText("Patient updated");
                }catch (IllegalArgumentException | NullPointerException e){
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Could not update");
                    alert.setHeaderText("Could update to the given values");
                    alert.setContentText(e.getMessage());
                    alert.showAndWait();
                }
            }
        }
    }

    /**
     * Removes the selected patient in the TableView from the register.
     * Before deletion the user is asked if this is its intention, and gives the opportunity to opt out.
     * If the patient for some reason could not be removed, the user is also notified.
     */
    @FXML
    public void removePatient(){
        Patient selectedPatient = tableView.getSelectionModel().getSelectedItem();
        if(selectedPatient == null){
             new NoPatientSelectedAlert().showAndWait();
        }else {

            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Delete confirmation");
            alert.setHeaderText("Delete confirmation");
            alert.setContentText("Are you sure you want to delete the following patient:" +
                    "\n" + selectedPatient.getFirstName()+ " " + selectedPatient.getLastName()+
                    "\n" + selectedPatient.getSocialSecurityNumber());

            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent()) {
                if (result.get() == ButtonType.OK) {
                   try{
                       PatientRegister updated = App.getPatientRegister();
                       updated.removePatient(selectedPatient.getSocialSecurityNumber());
                       App.setPatientRegister(updated);
                       updateTableView();
                       status.setText("Patient removed");
                   }catch(RemoveException e){
                       alert = new Alert(Alert.AlertType.INFORMATION);
                       alert.setTitle("Error");
                       alert.setHeaderText("Could not remove the selected patient");
                       alert.setContentText("");
                       alert.showAndWait();
                   }
                } else {
                    alert.close();
                }
            }
        }
    }

    /**
     * Shows a fileChooser window from where the user can select the wanted file to import data from.
     * If the selected file is of the wrong filetype, the user is notified and is given the
     * choice to choose again or cancel.
     * If the application is not able to retrieve data from the file, the user is notified.
     */
    @FXML
    public void importFromCSV(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select a file");
        File file = fileChooser.showOpenDialog(new Stage());

        CSVReader reader = new CSVReader();

        if(!file.getName().contains(".csv")){
            Alert alert = new Alert(AlertType.CONFIRMATION);
            ((Button) alert.getDialogPane().lookupButton(ButtonType.OK)).setText("Select file");
            alert.initModality(Modality.APPLICATION_MODAL);
            alert.setTitle("Wrong filetype");
            alert.setHeaderText("The chosen file type is not valid.");
            alert.setContentText("Please choose another file by clicking on Select File or cancel the operation");
            Optional<ButtonType> result = alert.showAndWait();
            if(result.isPresent() && result.get() == ButtonType.OK){
                importFromCSV();
            }
        }else{
            try{
                App.setPatientRegister(reader.readRegister(file));
                status.setText("Import successful");
                updateTableView();
            }catch(IOException | FileTypeException e){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.initModality(Modality.APPLICATION_MODAL);
                alert.setTitle("Error importing");
                alert.setHeaderText(e.getMessage());
                alert.showAndWait();
            }
        }
    }

    /**
     * Opens a saveDialog for the user to select filename and location for the data to be saved.
     * If the application is unable to write the data to the wanted file, the user is notified.
     * The user is also notified if the export was successful.
     */
    @FXML
    public void exportToCSV() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(".csv", "*.csv"));
        File file = fileChooser.showSaveDialog(new Stage());

        if(file != null) {
            try {
                new CSVWriter().writeRegister(App.getPatientRegister(),file);

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.initModality(Modality.APPLICATION_MODAL);
                alert.setTitle("Information Dialog - Export");
                alert.setHeaderText("Register exported to " + file.getName());
                alert.setContentText("The file is located in:" + "\n" + file.getPath());
                alert.showAndWait();

                status.setText("Export successful");
            } catch (IOException | NullPointerException | FileTypeException e) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.initModality(Modality.APPLICATION_MODAL);
                alert.setTitle("Error exporting");
                alert.setHeaderText(e.getMessage());
                alert.showAndWait();
            }
        }
    }

    /**
     * Method for closing application
     */
    @FXML
    private void exit(){
        App.exit();
    }

    /**
     * Sets the values of the TableView columns.
     */
    private void columnFactory(){
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        diagnosisColumn.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));
        generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
    }

    /**
     * Updates the TableView to the current state of the PatientRegister in the App class.
     */
    public void updateTableView(){
        tableView.setItems(FXCollections.observableList(new ArrayList<>(App.getPatientRegister().getPatients().values())));
        tableView.refresh();
    }
}