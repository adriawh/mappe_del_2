package ntnu.adriawh.exceptions;

public class AddException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     *
     * @param error the errormessage the developer wants to display when thrown
     */
    public AddException(String error) {
        super(error);
    }
}