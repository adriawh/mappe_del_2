package ntnu.adriawh.exceptions;

public class RemoveException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     *
     * @param error the errormessage the developer wants to display when thrown
     */
    public RemoveException(String error) {
        super(error);
    }
}
