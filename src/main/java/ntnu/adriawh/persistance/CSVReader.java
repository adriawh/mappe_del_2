package ntnu.adriawh.persistance;

import ntnu.adriawh.exceptions.AddException;
import ntnu.adriawh.exceptions.FileTypeException;
import ntnu.adriawh.model.Patient;
import ntnu.adriawh.model.PatientRegister;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class CSVReader {

    /**
     * Method for reading a register from a stored csv file.
     *
     * @param file the file to read data from
     * @return The register read from the given file
     * @throws IOException thrown if there was an error reading the file
     * @throws FileTypeException thrown if the filetype isn't csv
     */
    public PatientRegister readRegister(File file) throws IOException, FileTypeException {
        if(!file.getName().contains(".csv")){
            throw new FileTypeException("The given file is not csv");
        }
        PatientRegister loadedRegister = new PatientRegister();

        try (BufferedReader csvReader = new BufferedReader(new FileReader(file))) {
            String patientData;
            while ((patientData = csvReader.readLine()) != null) {
                String[] data = patientData.split(";");
                try {
                    if (data.length == 4) {
                        loadedRegister.addPatient(new Patient(data[0], data[1], data[2], "", data[3]));
                    } else if (data.length == 5) {
                        loadedRegister.addPatient(new Patient(data[0], data[1], data[2], data[3], data[4]));
                    }
                } catch (AddException | IllegalArgumentException ignored) {
                    //If a line of patientData contains data that cant be registered it is skipped
                }
            }
        }
        return loadedRegister;
    }
}
