package ntnu.adriawh.persistance;

import ntnu.adriawh.exceptions.FileTypeException;
import ntnu.adriawh.model.Patient;
import ntnu.adriawh.model.PatientRegister;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CSVWriter {

    /**
     * Method for writing a PatientRegister to a csv file.
     *
     * @param register the register to be stored
     * @param file the file to which the register is to be stored at
     * @throws IOException thrown if the FileWriter is unable to write to the given file
     * @throws NullPointerException thrown if the register to be stored is empty
     * @throws FileTypeException thrown if the filetype isn't csv
     */
    public void writeRegister(PatientRegister register, File file) throws IOException, NullPointerException,FileTypeException {
        if(!file.getName().contains(".csv")){
            throw new FileTypeException("Wrong filetype");
        }else if(register.getPatients().size() == 0){
            throw new NullPointerException("The register is empty");
        }
        try (FileWriter fileWriter = new FileWriter(file)) {
            for (Patient p : register.getPatients().values()) {
                fileWriter.append(p.getFirstName()).append(";")
                        .append(p.getLastName()).append(";")
                        .append(p.getGeneralPractitioner()).append(";")
                        .append(p.getDiagnosis()).append(";")
                        .append(p.getSocialSecurityNumber()).append(";")
                        .append("\n");
            }
        }
    }
}
