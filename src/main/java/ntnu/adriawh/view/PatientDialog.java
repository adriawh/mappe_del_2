package ntnu.adriawh.view;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import ntnu.adriawh.model.Patient;

/**
 * Dialog used to, create-, edit- and show info about a patient.
 */
public class PatientDialog extends Dialog<Patient> {

    public enum Mode {
        NEW, EDIT, INFO
    }

    private final Mode mode;

    private Patient existingPatient= null;

    /**
     * Constructor for adding a new patient
     */
    public PatientDialog() {
        super();
        this.mode = Mode.NEW;

        createDialog();
    }

    /**
     * Constructor for editing or showing info about an existing patient.
     * @param patient the patient to be edited or shown.
     * @param editable Defines if the dialog is to edit og not.
     */
    public PatientDialog(Patient patient, boolean editable) {
        super();
        if (editable) {
            this.mode = Mode.EDIT;
        } else {
            this.mode = Mode.INFO;
        }
        this.existingPatient = patient;

        createDialog();
    }

    /**
     * Method for generating the dialog based on the preferred mode
     */
    private void createDialog() {

        //Sets the title of the dialog based on what is to be shown
        switch (this.mode) {
            case NEW:
                setTitle("Patient Details - Add");
                break;
            case EDIT:
                setTitle("Patient Details - Edit");
                break;
            case INFO:
                setTitle("Patient Details");
                break;
            default:
                setTitle("UNKNOWN MODE");
                break;
        }

        // Set the button types.
        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        //Grid to organize inputs
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField firstName = new TextField();
        firstName.setPromptText("Required");
        TextField lastName = new TextField();
        lastName.setPromptText("Required");
        TextField socialSecurityNumber = new TextField();

        //User can only write numbers as social security number
        socialSecurityNumber.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                socialSecurityNumber.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        socialSecurityNumber.setPromptText("Cannot be edited later");
        TextField diagnosis = new TextField();
        TextField generalPractitioner = new TextField();

        if ((mode == Mode.EDIT) || (mode == Mode.INFO)) {
            //Fills in the information from the existing patient
            firstName.setText(existingPatient.getFirstName());
            lastName.setText(existingPatient.getLastName());
            socialSecurityNumber.setText(existingPatient.getSocialSecurityNumber());
            socialSecurityNumber.setDisable(true);
            socialSecurityNumber.setOpacity(0.8);
            grid.add(new Label("(Not editable)"),3,3);
            diagnosis.setText(existingPatient.getDiagnosis());
            generalPractitioner.setText(existingPatient.getGeneralPractitioner());
            if (mode == Mode.INFO) {
                //Disables the input fields
                firstName.setDisable(true);
                firstName.setOpacity(0.8);
                lastName.setDisable(true);
                lastName.setOpacity(0.8);
                socialSecurityNumber.setDisable(true);
                socialSecurityNumber.setOpacity(0.8);
                diagnosis.setDisable(true);
                diagnosis.setOpacity(0.8);
                generalPractitioner.setDisable(true);
                generalPractitioner.setOpacity(0.8);
            }
        }

        //Left side labels showing what the fields stand for
        Label firstNameLabel= new Label("First name: ");
        Label lastNameLabel= new Label("Last name: ");
        Label socialSecurityLabel= new Label("Social security number: ");
        Label diagnosisLabel= new Label("Diagnosis: ");
        Label generalPractitionerLabel= new Label("General practitioner: ");

        //Filling the grid with the input fields and labels
        grid.add(firstNameLabel, 1, 1);
        grid.add(firstName, 2, 1);
        grid.add(lastNameLabel, 1, 2);
        grid.add(lastName, 2, 2);
        grid.add(socialSecurityLabel, 1, 3);
        grid.add(socialSecurityNumber, 2, 3);
        grid.add(diagnosisLabel, 1, 4);
        grid.add(diagnosis, 2, 4);
        grid.add(generalPractitionerLabel, 1, 5);
        grid.add(generalPractitioner, 2, 5);

        getDialogPane().setContent(grid);

        //Dialog wont close unless all necessary fields are correctly filled
        getDialogPane().lookupButton(ButtonType.OK).addEventFilter(ActionEvent.ACTION, actionEvent -> {

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Could not update");
            alert.setHeaderText("Error");

            if (socialSecurityNumber.getText().length() != 11) {
                alert.setContentText("Social security number must be 11 numbers");
                alert.showAndWait();
                actionEvent.consume();
            }else if(firstName.getText().equals("") || lastName.getText().equals("")){
                alert.setContentText("A patient must have a first and a last name");
                alert.showAndWait();
                actionEvent.consume();
            }
        });

         //Sets the result of the the dialog to the data from the user
        setResultConverter((ButtonType button) -> {
            Patient result = null;
            if (button == ButtonType.OK) {
                if (mode == Mode.NEW) {
                    result = new Patient(firstName.getText(), lastName.getText(),
                            generalPractitioner.getText(), diagnosis.getText(), socialSecurityNumber.getText());
                } else if (mode == Mode.EDIT) {
                    existingPatient.setFirstName(firstName.getText());
                    existingPatient.setLastName(lastName.getText());
                    existingPatient.setDiagnosis(diagnosis.getText());
                    existingPatient.setGeneralPractitioner(generalPractitioner.getText());
                    result = existingPatient;
                }
            }
            return result;
        });
    }
}
