package ntnu.adriawh.view;

import javafx.scene.control.Alert;

/**
 * Alert shown to the user if no patient is selected when trying to execute a method where
 * a selected patient is needed.
 */
public class NoPatientSelectedAlert extends Alert {
    public NoPatientSelectedAlert() {
        super(AlertType.INFORMATION);
        setTitle("Select a patient:");
        setHeaderText("No patient selected" );
        setContentText("Please select a patient to remove");
    }
}
