package ntnu.adriawh.model;

import ntnu.adriawh.exceptions.AddException;
import ntnu.adriawh.exceptions.RemoveException;

import java.util.HashMap;

/**
 * Class for storing a multiple of patient object's
 */

public class PatientRegister{

    private HashMap<String, Patient> patients;

    public PatientRegister() {
        this.patients = new HashMap<>();
    }

    public HashMap<String, Patient> getPatients() {
        return patients;
    }

    /**
     * Adds a given patient to the register
     * @param patient the patient to be added to the register
     * @throws NullPointerException if the given object is Null
     * @throws AddException if there already exists a patient with the given social security number
     * @throws IllegalArgumentException if there is no social security number given with the patient
     */

    public void addPatient(Patient patient) throws NullPointerException, AddException, IllegalArgumentException{
        if(patient == null){
            throw new NullPointerException("No patient was given");
        }
        if(patient.getSocialSecurityNumber().equals("")||patient.getFirstName().equals("")||patient.getLastName().equals("")){
            throw new IllegalArgumentException("A patient must be registered with a social" +
                    " security number, first name and last name");
        }
        if(!patients.containsKey(patient.getSocialSecurityNumber())){
            patients.put(patient.getSocialSecurityNumber(), patient);
        }else{
            throw new AddException("There already exists a patient with the given social security number");
        }
    }

    /**
     * Removes a given patient from the register
     * @param socialSecurityNumber the social security number of the patient to be removed
     * @throws RemoveException thrown if there is no patient to remove with the given social security number
     */

    public void removePatient(String socialSecurityNumber) throws RemoveException {
        if(patients.containsKey(socialSecurityNumber)){
            patients.remove(socialSecurityNumber);
        }else {
            throw new RemoveException("No patient with the given social security number exists in the register");
        }
    }

    /**
     * Updates the values of a given patient
     * @param oldPatient the old value of the patient, which also tells the method which patient to update
     * @param updatedPatient the updated value of the patient
     * @throws NullPointerException thrown if there are no patients to update based on the oldPatient values
     */

    public void updatePatient(Patient oldPatient, Patient updatedPatient) throws NullPointerException, IllegalArgumentException{
        if(updatedPatient.getLastName().equals("") || updatedPatient.getFirstName().equals("")){
            throw new IllegalArgumentException("A patient must have a first and last name");
        }
        if(patients.containsKey(oldPatient.getSocialSecurityNumber())){
            patients.replace(oldPatient.getSocialSecurityNumber(), updatedPatient);
        }else {
            throw new NullPointerException("No patient with the given social security number exists in the register");
        }
    }

    @Override
    public String toString() {
        return "PatientRegister{" +
                "patients=" + patients +
                '}';
    }
}
