package ntnu.adriawh.model;

import java.util.Objects;


/**
 * Class for storing information about a Patient
 */
public class Patient {
    private final String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;

    /**
     *
     * @param firstName the first name of the patient, this is a required field
     * @param lastName the last name of the patient, this is a required field
     * @param generalPractitioner the general practitioner of the patient
     * @param diagnosis the diagnosis of the patient
     * @param socialSecurityNumber the social security number of the patient
     * @throws IllegalArgumentException if the patient is missing a first- or/and last name,
     * or if the given social security number is not with 11 numbers.
     */

    public Patient(String firstName, String lastName,String generalPractitioner, String diagnosis, String socialSecurityNumber) throws IllegalArgumentException{
        if(socialSecurityNumber.length() != 11){
            throw new IllegalArgumentException("Social security number must be of 11 numbers");
        }else if(lastName.equals("") || firstName.equals("")){
            throw new IllegalArgumentException("A patient need a first and last name");
        }
        this.socialSecurityNumber = socialSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName the new first name of the patient
     * @throws IllegalArgumentException if there is no name
     */
    public void setFirstName(String firstName) throws  IllegalArgumentException{
        if(firstName.length() > 0){
            this.firstName = firstName;
        }else {
            throw new IllegalArgumentException("First name cannot be empty");
        }
    }

    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName new last name of the patient
     * @throws IllegalArgumentException if there is no name
     */
    public void setLastName(String lastName) throws IllegalArgumentException{
        if(lastName.length() > 0){
            this.lastName = lastName;
        }else {
            throw new IllegalArgumentException("Last name cannot be empty");
        }
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * @param o other Patient object to compare
     * @return true if the two patient objects have the same social security number
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return Objects.equals(socialSecurityNumber, patient.socialSecurityNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Patient{" +
                "socialSecurityNumber='" + socialSecurityNumber + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                ", generalPractitioner='" + generalPractitioner + '\'' +
                '}';
    }
}
