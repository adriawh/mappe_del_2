package ntnu.adriawh.model;

import ntnu.adriawh.exceptions.AddException;
import ntnu.adriawh.exceptions.RemoveException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for PatientRegister.java
 */
public class PatientRegisterTest {

    /**
     * All tests for the addPatient method
     */
    @Nested
    public class addPatient{
        @Test
        @DisplayName("Adding a correctly generated patient")
        public void addPatientPositiveTest() {
            PatientRegister register = new PatientRegister();
            Patient test = new Patient("test", "Testesen", "Lege", "Kreft", "12345678901");
            try{
                register.addPatient(test);
            }catch(AddException e){
                fail(e);
            }
            assertTrue(register.getPatients().containsValue(test));
        }

        @Nested
        public class addPatientNegative{
            @Test
            @DisplayName("Adding a patient with an existing social security number")
            public void addPatientWithExistingSocialSecurityNumber() throws AddException {
                PatientRegister register = new PatientRegister();
                Patient test1 = new Patient("test", "Testesen", "Lege", "Kreft", "12345678901");
                Patient test2 = new Patient("testina", "Testesenine", "Lege", "Kreft", "12345678901");

                register.addPatient(test1);

                assertThrows(AddException.class, ()-> register.addPatient(test2));
            }

            @Test
            @DisplayName("Adding null")
            public void addPatientNullInput(){
                PatientRegister register = new PatientRegister();
                assertThrows(NullPointerException.class, ()-> register.addPatient(null));
            }
        }
    }

    /**
     * All tests for the removePatient method
     */
    @Nested
    public class removePatient{
        @Test
        @DisplayName("Removing an existing patient")
        public void removePatientPositive() throws AddException {
            PatientRegister register = new PatientRegister();
            Patient test = new Patient("test", "Testesen", "Lege", "Kreft", "12345678901");

            register.addPatient(test);

            try{
                register.removePatient(test.getSocialSecurityNumber());
            }catch (RemoveException e){
                fail(e);
            }

            assertFalse(register.getPatients().containsValue(test));
        }

        @Test
        @DisplayName("Removing a non existing patient")
        public void removePatientNegative() {
            PatientRegister register = new PatientRegister();
            assertThrows(RemoveException.class, ()-> register.removePatient("1"));
        }
    }


    /**
     * All tests for the update patient method
     */
    @Nested
    public class updatePatient {

        @Test
        @DisplayName("Updating an existing patient with correct data")
        public void updatePatientPositive() throws AddException {
            PatientRegister register = new PatientRegister();
            Patient test = new Patient("test", "Testesen", "Lege", "Kreft", "12345678901");

            register.addPatient(test);

            Patient updatedTest = new Patient("Tester", "Wihu", "Lege", "Halsbrann", "12345678901");

            try {
                register.updatePatient(test, updatedTest);
            }catch (NullPointerException | IllegalArgumentException e){
                fail(e);
            }

            assertEquals(register.getPatients().get("12345678901"), updatedTest);
        }

        @Test
        @DisplayName("Updating a non existing patient")
        public void updateNonExistingPatient() {
            PatientRegister register = new PatientRegister();
            Patient test = new Patient("test", "Testesen", "Lege", "Kreft", "12345678901");

            Patient updatedTest = new Patient("test", "wihu", "Lege", "Halsbrann", "12345678901");

            assertThrows(NullPointerException.class, () -> register.updatePatient(test, updatedTest));
        }
    }
}