package ntnu.adriawh.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Class for testing the patient class.
 */
public class PatientTest {

    /**
     * Negative testing for the constructor
     */
    @Nested
    public class newPatientNegative{
        @Test
        @DisplayName("Creating a patient with an illegal social security number")
        public void creatingPatientWithIllegalSocialSecurityNumber(){
            assertThrows(IllegalArgumentException.class, ()->new Patient("Test","Test", "","", "12345678"));
        }

        @Test
        @DisplayName("Creating a patient with no name")
        public void creatingPatientWithNoName(){
            assertThrows(IllegalArgumentException.class, ()->new Patient("","", "","", "12345678901"));
        }
    }
}