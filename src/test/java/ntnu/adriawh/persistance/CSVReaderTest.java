package ntnu.adriawh.persistance;

import ntnu.adriawh.exceptions.FileTypeException;
import ntnu.adriawh.model.Patient;
import ntnu.adriawh.model.PatientRegister;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class CSVReaderTest {

    @Test
    @DisplayName("Reading from a working csv file with correctly stored data")
    public void readRegisterPositive(){
        CSVReader reader = new CSVReader();
        File file = new File("src/test/java/ntnu/adriawh/persistance/onePatient.csv");
        PatientRegister register = new PatientRegister();

        try{
             register = reader.readRegister(file);
        }catch (IOException | FileTypeException e){
            fail(e);
        }

        assertTrue(register.getPatients().containsValue(new Patient("test", "test", "test", "test", "12345678901")));
    }

    @Nested
    public class readRegisterNegative{

        @Test
        @DisplayName("Reading from an unsupported filetype")
        public void readRegisterFromUnsupportedFileType() {
            CSVReader reader = new CSVReader();
            File file = new File("src/test/java/ntnu/adriawh/persistance/PatientRegisterTest.java");

            assertThrows(FileTypeException.class, ()->reader.readRegister(file));
        }

        @Test
        @DisplayName("Reading from file with faulty data")
        public void readRegisterWithPatientWithoutSocialSecurityNumber() throws IOException, FileTypeException {
            CSVReader reader = new CSVReader();
            File file = new File("src/test/java/ntnu/adriawh/persistance/test.csv");

            PatientRegister register = reader.readRegister(file);
            assertEquals(2,register.getPatients().size());
        }
    }
}