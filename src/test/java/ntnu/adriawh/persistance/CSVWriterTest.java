package ntnu.adriawh.persistance;

import ntnu.adriawh.exceptions.AddException;
import ntnu.adriawh.exceptions.FileTypeException;
import ntnu.adriawh.model.Patient;
import ntnu.adriawh.model.PatientRegister;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class CSVWriterTest {

    @Test
    @DisplayName("Writing an existing register to correct file type")
    public void writeRegisterPositive() throws IOException, AddException {
        PatientRegister register = new PatientRegister();
        Patient test = new Patient("test", "test", "test", "test", "12345678901");

        register.addPatient(test);

        CSVWriter writer = new CSVWriter();
        File file = new File("src/test/java/ntnu/adriawh/persistance/writeTest.csv");

        try{
            writer.writeRegister(register, file);
        }catch (IOException | FileTypeException | NullPointerException e){
            fail(e);
        }
        BufferedReader reader = new BufferedReader(new FileReader(file));
        assertEquals("test;test;test;test;12345678901;", reader.readLine());
    }

    @Nested
    @DisplayName("Negative tests")
    public class writeRegisterNegative{

        @Test
        @DisplayName("Writing an empty register")
        public void writeEmptyRegister(){
            PatientRegister register = new PatientRegister();

            CSVWriter writer = new CSVWriter();
            File file = new File("ntnu/adriawh/persistance/writeTest.csv");

            assertThrows(NullPointerException.class,()-> writer.writeRegister(register, file));
        }

        @Test
        @DisplayName("Writing register to an illegal file type")
        public void writeRegisterWithWrongFiletype() throws AddException {
            PatientRegister register = new PatientRegister();

            register.addPatient(new Patient("test", "test", "test", "test", "12345678901"));

            CSVWriter writer = new CSVWriter();
            File file = new File("ntnu/adriawh/persistance/writeTest.error");

            assertThrows(FileTypeException.class,()-> writer.writeRegister(register, file));
        }

        @Test
        @DisplayName("Writing register to a non file filepath")
        public void writeRegisterWithFilePathToDirectory(){
            PatientRegister register = new PatientRegister();

            CSVWriter writer = new CSVWriter();
            File file = new File("ntnu/adriawh/persistance/writeTest");

            assertThrows(FileTypeException.class,()-> writer.writeRegister(register, file));
        }
    }
}